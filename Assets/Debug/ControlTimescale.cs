﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlTimescale : MonoBehaviour {

	public float verySlowTimescale = 0.1f;
	public float slowTimescale = 0.6f;
	public float regularTimescale = 1.0f;

	void Update () {
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			SetTimescale (verySlowTimescale);
		}
		if (Input.GetKeyDown(KeyCode.Alpha2)) {
			SetTimescale (slowTimescale);
		}
		if (Input.GetKeyDown(KeyCode.Alpha0)) {
			SetTimescale (regularTimescale);
		}
	}

	void SetTimescale (float timescale) {
		Time.timeScale = timescale;
		Time.fixedDeltaTime = timescale * 0.02f;
	}
}
