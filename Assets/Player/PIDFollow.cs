﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PIDFollow : MonoBehaviour {

	public Transform target;
	
	public PIDVector pid;

	//Sword runs on fixed update to make fast swings more consistent
	void FixedUpdate () {
		GetComponent<Rigidbody>().AddForce ( pid.Update (target.position, transform.position, Time.fixedDeltaTime));
	}
}
