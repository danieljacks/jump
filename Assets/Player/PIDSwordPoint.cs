﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PIDSwordPoint : MonoBehaviour {

	public GameObject swordTrailPrefab;

	private GameObject swordTrailObj;

	void Start () {
		swordTrailObj = (GameObject) Instantiate (swordTrailPrefab, transform.position, Quaternion.identity);
		swordTrailObj.GetComponent<PIDFollow> ().target = gameObject.transform;
		//PIDFollow follow = swordTrailObj.GetComponent<PIDFollow>();
	}

	void OnDestroy () {
		Destroy (swordTrailObj);
	}
}
