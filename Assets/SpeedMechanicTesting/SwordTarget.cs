﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordTarget : MonoBehaviour {

	public GameObject swordTrailPrefab;

	private GameObject swordTrailObj;

	void Start () {
		swordTrailObj = (GameObject) Instantiate (swordTrailPrefab, transform.position, Quaternion.identity);
		swordTrailObj.GetComponent<SwordPoint> ().targetT = this.transform;
	}

	void OnDestroy () {
		Destroy (swordTrailObj);
	}
}
