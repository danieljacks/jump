﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTarget : MonoBehaviour {

	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			UpdateTargetPos ();
		}
	}

	private void UpdateTargetPos () {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit)) {
			transform.position = new Vector3 (hit.point.x, hit.point.y, transform.position.z);
		}
	}
}
