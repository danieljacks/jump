﻿/*
WHICHEVER WAY PLAYER ROTATES TO FACE TARGET, SHOULD BE SAME WAY PLAYER ROTATES TO FACE AWAY FROM TARGET AT END

enemy should have timer to rethink so it doesnt get stuck

AttackSlowdown should have something to let it smoothly slowdown, even if an enemy moves in the way when player is very close
    Really need this smoothing system, for other situations too. Maybe change set function to use a coroutine and target value, so only target value is directly changed. Everything is lerped to target value.
 
Make some way to slowdown blade, i.e. exert force if it will overshoot

 TODO:
 Find and record good values for spring-based sword for each sword type, then figure out function that can interpolate smoothly sword properties (size/weight, player strength) and give good spring values.
    Maybe graph points and see what comes out...

Get smoothed trails, something like this: https://www.assetstore.unity3d.com/en/#!/content/16076
 
 */