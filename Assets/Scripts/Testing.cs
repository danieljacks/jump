﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour {

	private float playerRadius;

	// Use this for initialization
	void Start () {
		playerRadius = GetComponent<PlayerMove>().playerRadius;
	}
	
	// Update is called once per frame
	void Update () {
		//FindCrouchPos (transform.position, transform.forward);
	}

	private Vector3 FindCrouchPos (Vector3 playerPos, Vector3 playerDir) {
		RaycastHit hit;

		Debug.DrawRay(playerPos, playerPos * -1);
		Ray ray = new Ray (playerPos, playerDir * -1);

		if (Physics.SphereCast(ray, playerRadius * 0.5f, out hit, playerRadius * 2.0f)) {
			//Vector3 posToHit = playerPos - hit.point;
			return Vector3.Lerp (playerPos, hit.point, 0.8f); //percent of way to wall
		} else {
			return playerDir * -1 * playerRadius * 0.5f; //no wall behind, default crouch dist
		}
	}
}
