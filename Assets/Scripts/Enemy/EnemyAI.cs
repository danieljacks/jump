﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(EnemyMove))]
public class EnemyAI : MonoBehaviour {

	public float rotSpeed = 10.0f;
	public float aimTime = 2.0f;
	public float thinkTime = 1.0f;
	public float minHeight = 1.0f;
	public float maxHeight = 5.0f;

	public GameObject bulletPrefab;

	private EnemyMove enemyMoveScr;
	private Transform player;
	private Coroutine runningActionRoutine;
	private bool isActing;
	private bool isTrackingPlayer;
	private Coroutine rethinkRoutineInstance;

	void Start () {
		enemyMoveScr = GetComponent<EnemyMove> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;

		Rethink ();
	}
	
	void Update () {
		if (isTrackingPlayer == true) {
			RotateTowardsPlayer ();
		}
	}

	public void Rethink () {
		if (rethinkRoutineInstance != null) StopCoroutine (rethinkRoutineInstance);
		StartCoroutine (RethinkRoutine());
	}

	private IEnumerator RethinkRoutine () {
		isActing = true;
		yield return new WaitForSeconds (thinkTime);
		if (UnityEngine.Random.Range (0,2) == 3) {
			isTrackingPlayer = true;
			yield return new WaitForSeconds (aimTime);
			Fire (player.position);
			isTrackingPlayer = false;
			Rethink ();
		} else {
			GetComponent<EnemyMove> ().MoveRandom (minHeight, maxHeight);
		}

	}

	private void Fire (Vector3 targetPos) {
		Projectile proj = Instantiate (bulletPrefab, transform.position, transform.rotation).GetComponent<Projectile> ();
		proj.targetPos = targetPos;
	}

	private void RotateTowardsPlayer () {
		Quaternion desiredRot = Quaternion.LookRotation (player.position - transform.position, Vector3.up);
		transform.rotation = Quaternion.RotateTowards (transform.rotation, desiredRot, rotSpeed * Time.deltaTime);
	}
}
