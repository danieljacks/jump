﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour {

	public float speed;
	public float range;
	public float damage;
	[HideInInspector]
	public Vector3 targetPos;

	private Vector3 direction;
	private Vector3 initialPos;


	private void Start () {
		direction = (targetPos - transform.position).normalized;
	}

	private void Update () {
		transform.position = Vector3.MoveTowards (transform.position, targetPos, speed * Time.deltaTime);
		transform.LookAt (targetPos, Vector3.up);
		
		float distanceTravelled = (transform.position - initialPos).magnitude;
		if (distanceTravelled > range) {
			Destroy (gameObject);
		}
	}

	private void OnCollisionEnter (Collision col) {
		if (col.gameObject.CompareTag("Player")) {
			col.gameObject.GetComponent<Health> ().Damage (damage);
			Destroy (gameObject);
		}
		if (col.gameObject.CompareTag("Environment")) {
			Destroy (gameObject);
		}
	}
}
