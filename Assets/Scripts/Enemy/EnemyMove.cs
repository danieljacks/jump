﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class EnemyMove : MonoBehaviour {

	public float climbSpeed = 1.0f;
	public Rect moveArea;
	public GameObject navAgentPrefab;
	public UnityEvent OnPathFinished;


	private GameObject navObj;
	private NavMeshAgent agent;
	private Coroutine changeHeightRoutine;
	[HideInInspector]
	public bool isMoving = false;


	private void Awake () {
		Vector3 navPos = transform.position; //Create nav agent to follow
		navPos.y = 0.2f;
		navObj = Instantiate (navAgentPrefab, navPos, transform.rotation);
		agent = navObj.GetComponent<NavMeshAgent> ();
	}

	private void Update () {
		Vector3 newPos = navObj.transform.position;
		newPos.y = transform.position.y;
		transform.position = newPos;

		if (isMoving == true && CheckPathFinished()) {
			isMoving = false;
			OnPathFinished.Invoke ();
		}
	}

	private bool CheckPathFinished () {
		if (!agent.pathPending)
		{
			if (agent.remainingDistance <= agent.stoppingDistance)
			{
				if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
				{
					return true;
				}
			}
		}
		return false;
	}

	/*protected void OnPathFinished (EventArgs e) {
		if (PathFinished != null) {
			PathFinished (this, e);
		}
	}*/

	public void MoveRandom (float heightMin, float heightMax) {
		isMoving = true;

		Vector3 position 	= new Vector3 (UnityEngine.Random.Range(moveArea.xMin, moveArea.xMax), 0, UnityEngine.Random.Range(moveArea.yMin, moveArea.yMax));
		agent.destination 	= position;

		float height = UnityEngine.Random.Range (heightMin, heightMax);

		if (changeHeightRoutine != null) StopCoroutine (changeHeightRoutine);
		changeHeightRoutine = StartCoroutine (ChangeHeight(height));
	}

	private IEnumerator ChangeHeight (float height) {
		float initialHeight = transform.position.y;
		float t = 0;

		do {
			t = Mathf.Clamp01 (t += Time.deltaTime * climbSpeed);
			float newHeight = Mathf.SmoothStep (initialHeight, height, t);
			transform.position = new Vector3 (transform.position.x, newHeight, transform.position.z);
			yield return null;
		} while (t <= 1);
	}
}
