﻿using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System;
using System.Linq; 

namespace Helpers {
public static class MathHelp {

	///Moves val by delta, wrapping it so that val remains within min_val and max_val
	public static int WrapAround (int val, int delta, int min_val, int max_val) {
		if (delta >= 0) {
			return (val + delta - min_val) % max_val + min_val;
		} else {
			return ((val + delta) + max_val * (-delta) - min_val) % max_val + min_val;
		}
	}

	///Moves val by delta, wrapping it so that val remains within min_val and max_val
	public static float WrapAround (float val, float delta, float min_val, float max_val) {
		if (val + delta >= min_val && val + delta <= max_val) { //Value already in correct range
			return val + delta;
		} 

		if (delta >= 0) {
			return (val + delta - min_val) % max_val + min_val;
		} else {
			return ((val + delta) + max_val * (-delta) - min_val) % max_val + min_val;
		}
	}

	///Wraps val so that it remains within min_val and max_val
	public static float WrapAround (float val, float min_val, float max_val) {
		return WrapAround (0, val, min_val, max_val);
	}

	///Wraps val so that it remains within min_val and max_val
	public static int WrapAround (int val, int min_val, int max_val) {
		int range_size = max_val - min_val + 1;

		if (val < min_val)
			val += range_size * ((min_val - val) / range_size + 1);

		return min_val + (val - min_val) % range_size;
	}

	///Wraps angle so that it remains within 0..359 range
	public static float WrapAngle (float angle) {
		return WrapAround (angle, 0, 359);
	}

	///Returns quadrant of angle (1..4)
	public static float Quadrant (float angle) {
		angle = WrapAngle (angle);

		float quadrant;
		if (angle >= 0 && angle < 90) {
			quadrant = 1;
		} else if (angle >= 90 && angle < 180) {
			quadrant = 2;
		} else if (angle >= 180 && angle < 270) {
			quadrant = 3;
		} else if (angle >= 270 && angle < 360) {
			quadrant = 4;
		} else {
			Debug.LogError ("Unrecognised error");
			return 1;
		}

		return quadrant;
	}

	///Returns -1 if the quadrant would give a negative sin, else returns 1
	public static float SinSign (float quadrant) {
		if (quadrant == 1 || quadrant == 2) {
			return 1;
		} else {
			return -1;
		}
	}

	///Returns -1 if the quadrant would give a negative cos, else returns 1
	public static float CosSign (float quadrant) {
		if (quadrant == 1 || quadrant == 4) {
			return 1;
		} else {
			return -1;
		}
	}

	///Returns the percentage completed of val through a..b
	public static float InverseLerp (Vector2 current, Vector2 start, Vector2 end) {
		//Parameters originally: a, b, val, hence naming scheme (AB, AV)
		Vector2 AB = start - end;
		Vector2 AV = current - start;
		return Vector2.Dot(AV, AB) / Vector2.Dot(AB, AB);
	}

	///Returns the direction vector of given angle
	public static Vector2 AngleToDirection (float angle) {
		float rads = angle * Mathf.Deg2Rad;
		Vector2 direction = new Vector2 (Mathf.Cos(rads), Mathf.Sin(rads));

		return direction;
	}

	///Returns the angle of given direction vector
	public static float DirectionToAngle (Vector2 direction) {
		float angle = Mathf.Acos (direction.x) * Mathf.Rad2Deg;

		return angle;
	}

	///Returns true if all components of given vectors are within given margin
	public static bool VectorSimilar (Vector2 a, Vector2 b, float margin) {
		if (a.x >= b.x - margin && a.x <= b.x + margin && a.y >= b.y - margin && a.y <= b.y + margin) {
			return true;
		} else {
			return false;
		}
	}

	public static bool FloatSimilar (float a, float b, float margin) {
		if (margin < 0) {
			margin *= -1;
		}

		if (a >= b - margin && a <= b + margin) {
			return true;
		} else {
			return false;
		}
	}

	public static float ProjectedAngle (Quaternion B, Quaternion A) {
		//Quaternion A; //first Quaternion - this is your desired rotation
		//Quaternion B; //second Quaternion - this is your current rotation


		// define an axis, usually just up
		Vector3 axis = new Vector3(0.0f, 1.0f, 0.0f);


		// mock rotate the axis with each quaternion
		Vector3 vecA = A * axis;
		Vector3 vecB = B * axis;


		// now we need to compute the actual 2D rotation projections on the base plane
		float angleA = Mathf.Atan2(vecA.x, vecA.z) * Mathf.Rad2Deg;
		float angleB = Mathf.Atan2(vecB.x, vecB.z) * Mathf.Rad2Deg;


		// get the signed difference in these angles
		var angleDiff = Mathf.DeltaAngle( angleA, angleB );
		return angleDiff;
	}

	///Returns a random grid-based position between inner and outer chebyshev radii. Inefficient implementationthough, I think. Equal radii will result in a single cell-think layer.
	public static Vector2 RandGridBetweenRadii (float innerRadius, float outerRadius, float gridSize) {
		if (innerRadius > outerRadius) {
			Debug.LogError ("Inner radius must be larger than or equal to outer radius");
			return Vector2.zero;
		}

		int innerRadiusGrid = Mathf.RoundToInt (innerRadius / gridSize); //radii in grid scale, i.e. 1 radius = 1 chebyshev grid unit distance
		int outerRadiusGrid = Mathf.RoundToInt (outerRadius / gridSize);

		//Everything from here done in grid units
		float outerDimensionsGrid = 2 * outerRadiusGrid + 1;
		int halfDim = Mathf.RoundToInt( outerDimensionsGrid * 0.5f);

		List<Vector2> cells = new List<Vector2> ();

		for (int y = -outerRadiusGrid ; y <= outerRadiusGrid; y++) {
			for (int x = -outerRadiusGrid; x <= outerRadiusGrid; x++) {

				if (ChebyshevDistance (new Vector2 (x, y), Vector2.zero) >= innerRadiusGrid) {
					cells.Add (new Vector2 (x, y));
					Debug.Log ("Adding: " + new Vector2 (x, y).ToString() );
				}
			}
		}

		Vector2 chosenCell = cells[UnityEngine.Random.Range(0, cells.Count)];

		//Convert from grid-space to worl-space
		chosenCell *= gridSize;

		return chosenCell;
	}

	///Tests whether there is anything in the specified tile
	public static bool AreaEmpty (Rect area) { 
		Collider2D[] cols = new Collider2D[1];

		if ( Physics2D.OverlapAreaNonAlloc(area.min, area.max, cols, Physics.AllLayers, 0, 0) == 0) { //Nothing there
			return true;
		} else {
			return false;
		}
	}

	///Returns the manhattan distance between current and destination
	public static float ManhattanDistance (Vector2 current, Vector2 destination) {
		return Mathf.Abs (destination.x - current.x) + Mathf.Abs (destination.y - current.y);
	}

	///Returns the Chebyshev distance between the two vector points
	public static float ChebyshevDistance (Vector2 current, Vector2 destination) {
		return Mathf.Max(Mathf.Abs(current.x - destination.x), Mathf.Abs(current.y - destination.y));
	}
}
}