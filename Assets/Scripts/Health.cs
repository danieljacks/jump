﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour {

	public float maxHealth = 100.0f;
	public float health = 100.0f;
	public UnityEvent OnDeath;


	private void Start () {
		health = Mathf.Clamp (health, 0, maxHealth);
	}

	public void Damage (float amount) {
		health = Mathf.Clamp (health - amount, 0, maxHealth);

		if (health <= 0) {
			OnDeath.Invoke ();
		}
	}

	public void Restore (float amount) {
		health = Mathf.Clamp (health + amount, 0, maxHealth);

		if (health <= 0) {
			OnDeath.Invoke ();
		}
	}


}
