﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackTriggers : MonoBehaviour {

	public string tag = "Enemy";

	private HashSet<GameObject> gos = new HashSet<GameObject> ();

	void OnTriggerEnter (Collider other) {
		if (other.CompareTag (tag) == true) {
			gos.Add (other.gameObject);
		}
	}

	void OnTriggerExit (Collider other) {
		gos.Remove (other.gameObject);
	}

	/// returns infinity if nothing in GameObject list
	public float GetClosestDist (Vector3 origin) {
		float shortestSqrDist = Mathf.Infinity;
		foreach (GameObject go in gos) {
			float sqrDist = (go.transform.position - origin).sqrMagnitude;
			if (sqrDist < shortestSqrDist) {
				shortestSqrDist = sqrDist;
			}
		}

		return Mathf.Sqrt (shortestSqrDist);
	}
}
