﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TrackTriggers))]
public class ProximitySlowdown : MonoBehaviour {
	public float startSlowdownDist = 5.0f;
	public float endSlowdownDist = 1.0f;
	public float maxSlowdown = 0.8f;
	public AnimationCurve slowdownCurve;

	private TrackTriggers trackTriggers;

	void Awake () {
		trackTriggers = GetComponent<TrackTriggers> ();
	}

	void Update () {
		float enemyDist = trackTriggers.GetClosestDist (transform.position);

		float lerpPercent = Mathf.InverseLerp (startSlowdownDist, endSlowdownDist, enemyDist);
		float inv = slowdownCurve.Evaluate (lerpPercent) * maxSlowdown;
		SetTimeSpeed (1 - inv);
	}

	void SetTimeSpeed (float speed) {
		Time.timeScale = speed;
		Time.fixedDeltaTime = speed * 0.02f;
	}
}
