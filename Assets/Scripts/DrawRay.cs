﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawRay : MonoBehaviour {

	public Vector3 origin = Vector3.zero;
	public Vector3 direction = Vector3.forward;
	public Color color = Color.blue;
	public float duration = 1.0f;

	void Update () {
		Debug.DrawRay (origin, direction, color, duration);
	}

	void OnDrawGizmos () {
		//Gizmos.DrawRay (origin, direction);
	}
}
