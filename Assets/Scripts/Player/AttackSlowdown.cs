﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AttackSlowdown : MonoBehaviour {
	public float startSlowdownDist = 4.5f;
	public float endSlowdownDist = 2.0f;
	public float maxSlowdown = 0.95f;
	public float sphereCastRadius = 0.2f;
	public AnimationCurve slowdownCurve;
	[Space]
	public float startSlowdownVelocity = 2.0f;
	public float fullSlowdownVelocity = 5.0f;


	private Rigidbody rBody;

	void Awake () {
		rBody = GetComponent<Rigidbody> ();
	}

	void Update () {
		RaycastHit hit;
		Ray ray = new Ray (transform.position, rBody.velocity.normalized);
		//Slowdown if there is an enemy in a collision path with the player
		if (Physics.SphereCast (ray, sphereCastRadius, out hit, startSlowdownDist) && hit.collider.gameObject.CompareTag("Enemy")) {
			float enemyDist = ( hit.point - transform.position ).magnitude;

			float lerpPercent = Mathf.InverseLerp (startSlowdownDist, endSlowdownDist, enemyDist);
			float curveValue = slowdownCurve.Evaluate (lerpPercent) * maxSlowdown;

			float speed = rBody.velocity.magnitude;
			float velocityPercent = Mathf.InverseLerp (startSlowdownVelocity, fullSlowdownVelocity, speed);
			float inverse = Mathf.Lerp (0, curveValue, velocityPercent);
			print ("velocityPercent: " + velocityPercent + " | slowdown: " + (1-inverse).ToString());
			SetTimeSpeed (1 - inverse);
		} else {
			SetTimeSpeed (1);
		}

		//print (rBody.velocity.magnitude);
	}

	void SetTimeSpeed (float speed) {
		Time.timeScale = speed;
		Time.fixedDeltaTime = speed * 0.02f;
	}
}
