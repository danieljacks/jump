﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCam : MonoBehaviour {

	public Vector2 sensitivity = new Vector2 (5, 5);

    private Camera cam;
    private Vector2 mouseRot;
    private Vector2 mousePrevPos;

    void Start () {
        cam = Camera.main;
        mousePrevPos = Input.mousePosition;
    }

    void Update () {
        Vector2 mouseDelta = (Vector2) Input.mousePosition - mousePrevPos;
        mousePrevPos = Input.mousePosition;

        mouseRot += Vector2.Scale (mouseDelta, sensitivity);
        cam.transform.rotation = Quaternion.Euler (mouseRot.y, mouseRot.x, 0);

    }
}
