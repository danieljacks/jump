﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SpringJoint))]
public class SwordPoint : MonoBehaviour {

	public float mass = 10.0f;
	public float strength = 10.0f;
	public float gravityStrength = 9.8f;
	public float gravityMultiplierMax = 2.0f;
	public float gravityMultiplierMaxDist = 2.0f;
	public float drag = 2.0f;
	public float damping = 0.2f;
	public Transform targetT;

	private Rigidbody rBody;
	private SpringJoint spring;
	private float gravityMultiplier;

	void Start () {
		rBody = GetComponent<Rigidbody> ();
		spring = GetComponent<SpringJoint> ();
		
		spring.connectedBody = targetT.gameObject.GetComponent<Rigidbody> ();

		rBody.useGravity = false;
		gravityMultiplier = gravityMultiplierMax;
	}

	void Update () {
		rBody.mass = mass;
		rBody.drag = drag;
		spring.spring = strength;
		spring.damper = damping;

		SetGravityMultiplier ();
	}

	void FixedUpdate () {
		rBody.AddForce (Vector3.down * gravityStrength * gravityMultiplier);
	}

	void SetGravityMultiplier () {
		float targetY = targetT.position.y;
		float boxY = transform.position.y;
		float yDiff =  boxY - targetY; // +ve indicates down
		if (yDiff <= 0) {
			gravityMultiplier = 1;
		} else {
			float t = Mathf.InverseLerp (0, gravityMultiplierMaxDist, yDiff);
			float multiplier = Mathf.Lerp (1, gravityMultiplierMax, t);
			gravityMultiplier = multiplier;
		}
	}
}
