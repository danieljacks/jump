﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerWalk : MonoBehaviour {
	public float walkForce = 5.0f;
	private Rigidbody rBody;

	void Awake () {
		rBody = GetComponent<Rigidbody> ();
	}

	void FixedUpdate () {
		rBody.AddForce (Vector3.right * Input.GetAxisRaw("Horizontal") * walkForce);
		rBody.AddForce (Vector3.forward * Input.GetAxisRaw("Vertical") * walkForce);
	}
}
