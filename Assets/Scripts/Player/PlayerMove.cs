﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Helpers;

public class PlayerMove : MonoBehaviour {

	public float rotationSpeed = 5.0f;
	public float playerRadius = 0.5f;
	
	[Space]
	[Header("Animation Curves")]
	public float crouchSpeed = 5.0f;
	public AnimationCurve crouchCurve;
	public float crouchPauseDuration;
	public float jumpSpeed = 5.0f;
	public float jumpShakeAsymptote = 2.0f;
	public AnimationCurve jumpCurve;
	public float jumpPauseDuration;
	public float landSpeed = 5.0f;
	public AnimationCurve landingCurve;

	[Space]
	public GameObject targetPrefab;

	private GameObject targetObj;
	private Camera cam;
	private bool grounded = true;
	private Coroutine jumpRoutineInstance;

	public GameObject TESTINGOBJ;
	public float TESTINGFLOAT;

	void Start() {
		cam = Camera.main;
	}

	void Update() {
		if (Input.GetMouseButtonDown(0)) {
			if (grounded == true) {
				RaycastHit hit;
				Ray ray = cam.ScreenPointToRay(Input.mousePosition);
				if (Physics.SphereCast(ray, playerRadius, out hit)) { //Check position
					if (targetObj != null) Destroy(targetObj);
					targetObj = Instantiate (targetPrefab, hit.point, Quaternion.LookRotation(hit.normal, Vector3.up)) as GameObject;
					
					//JUMP TO THE TARGET
					Target targetScr = targetObj.GetComponent<Target> ();
					StartJump (targetScr);
				}
			}
		}
	}

	private void StartJump (Target target) {
		if (jumpRoutineInstance !=  null)
			StopCoroutine (jumpRoutineInstance);
		jumpRoutineInstance = StartCoroutine (JumpRoutine(target));
	}

	private IEnumerator JumpRoutine (Target target) {
		//Setup points
		Vector3 startPos 		= transform.position;
		Vector3 jumpDir 		= (target.transform.position - startPos).normalized;

		Vector3 crouchPos 		= FindCrouchPos (startPos, jumpDir);
		Quaternion crouchRot 	= Quaternion.LookRotation (jumpDir, Vector3.up);

		Vector3 targetPos 		= target.standPos.position;
		Quaternion targetRot 	= target.standPos.rotation;

		Vector3 landPos 		= FindCrouchPos (targetPos, jumpDir * -1);
		Quaternion landRot		= targetRot;

		//run routines
		yield return StartCoroutine (Crouch(crouchSpeed, startPos, crouchPos, crouchRot));

		// pause
		float t = Time.timeScale;
		Time.timeScale = 0;
		yield return new WaitForSecondsRealtime (0.5f);
		Time.timeScale = t;

		yield return StartCoroutine (Jump(jumpSpeed, crouchPos, targetPos));

		yield return StartCoroutine (Land (landSpeed, targetPos, landPos, landRot));
	}

	private IEnumerator Crouch (float speed, Vector3 restPos, Vector3 crouchPos, Quaternion endRot) {
		Quaternion startRot = transform.rotation;
		float p = 0;
		do {
			p = Mathf.Clamp01 (p + Time.deltaTime * speed);
			transform.position = Vector3.LerpUnclamped (restPos, crouchPos, crouchCurve.Evaluate(p));
			transform.rotation = Quaternion.Slerp (startRot, endRot, p);
			yield return null;
		} while (p < 1);
	}

	private IEnumerator Jump (float speed, Vector3 startPos, Vector3 targetPos) {
		float dist = (targetPos - startPos).magnitude;
		float shakeAmount = (float)System.Math.Tanh ((double)dist * (double)TESTINGFLOAT) * jumpShakeAsymptote;
		cam.GetComponent<CameraShake>().ShakeCamera (shakeAmount, 0.035f / speed);

		float p = 0;

		do {
			p = Mathf.Clamp01 (p + Time.deltaTime * speed);
			transform.position = Vector3.LerpUnclamped (startPos, targetPos, jumpCurve.Evaluate(p));
			yield return null;
		} while (p < 1);
	}

	private IEnumerator Land (float speed, Vector3 targetPos, Vector3 landPos, Quaternion endRot) {
		Quaternion startRot = transform.rotation;
		float p = 0;
		do {
			p = Mathf.Clamp01 (p + Time.deltaTime * speed);
			transform.position = Vector3.LerpUnclamped (targetPos, landPos, landingCurve.Evaluate(p));
			transform.rotation = Quaternion.Slerp (startRot, endRot, p);
			yield return null;
		} while (p < 1);
	}

	private Vector3 FindCrouchPos (Vector3 playerPos, Vector3 playerDir) {
		RaycastHit hit;
		Ray ray = new Ray (playerPos, playerDir * -1);

		if (Physics.Raycast(ray, out hit, playerRadius * 2.0f)) {
			//Vector3 posToHit = playerPos - hit.point;
			//Instantiate (TESTINGOBJ, Vector3.Lerp (playerPos, hit.point, 0.8f), transform.rotation);
			return Vector3.Lerp (playerPos, hit.point, 0.8f); //percent of way to wall
		} else {
			//Instantiate (TESTINGOBJ, playerPos + playerDir * -1 * playerRadius * 2.0f, transform.rotation);
			return playerPos + playerDir * -1 * playerRadius * 2.0f; //no wall behind, default crouch dist
		}
	}

}
