﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PlayerCrouch))]
public class PlayerJump : MonoBehaviour {

	public float jumpForce = 5.0f;
	public float groundedCheckDistance = 1.0f;
	public float crouchPercent = 0.8f;

	// ------ Private variables ------

	private bool isGrounded = true;
	private bool isJumping = false;
	private Rigidbody rBody;
	private Camera cam;
	private Coroutine jumpRoutineInstance = null;
	private PlayerCrouch playerCrouch;

	void Start () {
		cam = Camera.main;
		rBody = GetComponent<Rigidbody> ();
		playerCrouch = GetComponent<PlayerCrouch>();
	}

	private void Update () {
		isGrounded = CheckGrounded ();
		
		if (Input.GetMouseButtonDown(0)) {
			if (isGrounded == true && isJumping == false) {
				Vector3 playerDir = cam.ScreenPointToRay(Input.mousePosition).direction;
				Jump (playerDir);
			}
		}
	}

	private bool CheckGrounded () {
		if (Physics.Raycast (transform.position, Vector3.down, groundedCheckDistance)) {
			return true;
		} else {
			return false;
		}
	}

	private void Jump (Vector3 playerDir) {
		if (jumpRoutineInstance != null) {
			StopCoroutine (jumpRoutineInstance);
		}
		jumpRoutineInstance = StartCoroutine (JumpRoutine (playerDir));
	}

	private IEnumerator JumpRoutine (Vector3 playerDir) {
		isJumping = true;
		yield return playerCrouch.CrouchIEnumerator (playerDir, crouchPercent);
		rBody.AddForce (playerDir * jumpForce, ForceMode.Impulse);
		isJumping = false;
		yield return playerCrouch.UncrouchIEnumerator ();

	}
}