﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCrouch : MonoBehaviour {

	public float playerRadius = 0.5f;
	[Tooltip("This, when multiplied by playerRadius give the maximum crouch distance")]
	public float radiusToMaxCrouchDistFactor = 2.0f;
	public float crouchSpeed = 2.0f;
	public AnimationCurve crouchCurve;
	public float uncrouchSpeed = 2.0f;
	public AnimationCurve uncrouchCurve;
	public Vector3 camLocalRestPos = Vector3.zero;
	public Transform camParentT;

	private Coroutine crouchRoutineInstance = null;
	private Coroutine uncrouchRoutineInstance = null;

	public void Crouch (Vector3 playerDir, float crouchPercent) {
		StartCoroutine (CrouchIEnumerator(playerDir, crouchPercent));
	}

	public IEnumerator CrouchIEnumerator (Vector3 playerDir, float crouchPercent) {
		if (crouchRoutineInstance != null) {
			StopCoroutine (crouchRoutineInstance);
		}
		crouchRoutineInstance = StartCoroutine (CrouchRoutine (playerDir, crouchPercent));

		yield return crouchRoutineInstance;
	}

	private IEnumerator CrouchRoutine (Vector3 playerDir, float crouchPercent) {
		Vector3 localRestPos = camLocalRestPos;
		Vector3 localCrouchPos = FindLocalCrouchPos (transform.position, playerDir, crouchPercent);

		Quaternion startRot = transform.rotation;
		float p = 0;
		do {
			p = Mathf.Clamp01 (p + Time.deltaTime * crouchSpeed);
			Camera.main.transform.localPosition = Vector3.LerpUnclamped (localRestPos, localCrouchPos, crouchCurve.Evaluate(p));
			//transform.rotation = Quaternion.Slerp (startRot, endRot, p);
			yield return null;
		} while (p < 1);
	}

	public void Uncrouch () {
		StartCoroutine (UncrouchIEnumerator ());
	}

	public IEnumerator UncrouchIEnumerator () {
		if (uncrouchRoutineInstance != null) {
			StopCoroutine (uncrouchRoutineInstance);
		}
		uncrouchRoutineInstance = StartCoroutine (UncrouchRoutine ());

		yield return uncrouchRoutineInstance;
	}

	private IEnumerator UncrouchRoutine () {
		Vector3 localRestPos = camLocalRestPos;
		Vector3 localCrouchPos = Camera.main.transform.localPosition;

		float p = 0;
		do {
			p = Mathf.Clamp01 (p + Time.deltaTime * crouchSpeed);
			Camera.main.transform.localPosition = Vector3.LerpUnclamped (localCrouchPos, localRestPos, uncrouchCurve.Evaluate(p));
			yield return null;
		} while (p < 1);
	}

	private Vector3 FindLocalCrouchPos (Vector3 playerPos, Vector3 playerDir, float crouchPercent) {
		RaycastHit hit;
		Ray ray = new Ray (playerPos, playerDir * -1);

		Vector3 worldPos;
		if (Physics.Raycast(ray, out hit, playerRadius * radiusToMaxCrouchDistFactor)) {
			worldPos = Vector3.Lerp (playerPos, hit.point, crouchPercent); //percent of way to wall
		} else {
			worldPos = playerPos + playerDir * -1 * playerRadius * radiusToMaxCrouchDistFactor; //no wall behind, default crouch dist
		}

		return Camera.main.transform.InverseTransformPoint (worldPos); //Everything is in relative coords
	}
}
