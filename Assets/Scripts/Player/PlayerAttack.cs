﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

	public GameObject swordPrefab;
	public AnimationCurve posCurveX;
	public AnimationCurve posCurveY;
	public AnimationCurve speedCurve;
	public Rect attackArea = new Rect (-0.5f, -0.5f, 1, 1);
	public float attackDistance = 0.5f;
	public float attackSpeed = 1.0f;

	private Transform swordT;
	private Coroutine attackRoutineInstance = null;
	private bool isAttacking = false;

	void Update () {
		if (Input.GetMouseButtonDown (0) && isAttacking == false) {
			Attack ();
		}
	}

	void Attack () {
		if (attackRoutineInstance != null) {
			StopCoroutine (attackRoutineInstance);
		}
		if (swordT != null) {
			Destroy (swordT.gameObject);
		}
		GameObject obj = (GameObject) Instantiate (swordPrefab, GetSwordPos (0), transform.rotation, transform);
		swordT = obj.transform;
		attackRoutineInstance = StartCoroutine (AttackRoutine ());
	}

	private IEnumerator AttackRoutine () {
		isAttacking = true;

		float t = 0;
		do {
			swordT.position = GetSwordPos (speedCurve.Evaluate(t)); // initial update ensures beginning of attack is animated
			yield return new WaitForFixedUpdate ();
			t += attackSpeed;
		} while (t < 1);

		yield return new WaitForFixedUpdate (); //Final update ensures end of attack is animated
		swordT.position = GetSwordPos (speedCurve.Evaluate(t));

		isAttacking = false;
	}

	private Vector3 GetSwordPos (float t) {
		Transform camT = Camera.main.transform;

		Vector3 pos;
		pos = camT.position;
		pos += camT.forward * attackDistance;
		pos += camT.right * -1 * attackArea.x;
		pos += camT.up * -1 * attackArea.y;
		pos += camT.right * attackArea.width * posCurveX.Evaluate (t);
		pos += camT.up * attackArea.height * posCurveY.Evaluate (t);

		return pos;
	}
}
